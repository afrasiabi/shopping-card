const express = require('express')
const cors = require('cors')
const fs = require('fs')

const app = express()
app.use(cors())

const port = 3001

app.get('/api/items', (req, res) => {
  fs.readFile('./data/items.json', (err, json) => {
    let obj = JSON.parse(json)
    res.json(obj)
  })
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
