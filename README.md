# Whee shopping App

This is a Whee shopping application written in React, Typescript and Nodejs.

## Run project

### Step 1: Install dependencies

```bash
$ npm install
```

### Step 2: Run server

```bash
$ npm run serve
```

### Step 2: Run frontend in development mode

```bash
$ npm start
```

### Run tests

```bash
$ npm run test
```

### Run static code quality checker

```bash
$ npm run static-code-quality
```

Todo: This could be a part of CI pipelines or Git hooks.

### Future improvements

The current server is not something that could be used in production, it just has the most basic functionality for this example project.

There should be layers of tests and the current tests are missing some important coverage.

The app architecture, the application state and how the files are organized could be improved in future to be more scabale.

Application accessibilty could be considered as a future improvement. App could be localized and globalized.
