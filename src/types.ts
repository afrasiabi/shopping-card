export type ItemType = {
  id: number
  title: string
  description: string
  image: string
  price: number
  amount: number
}
