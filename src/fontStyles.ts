import { createGlobalStyle } from 'styled-components'
import Pacifico from './fonts/Pacifico-Regular.woff2'
import PlayfairItalic from './fonts/PlayfairDisplay-Italic.woff2'
import Playfair from './fonts/PlayfairDisplay.woff2'

const FontStyles = createGlobalStyle`

@font-face {
  font-family: 'Pacifico';
  src: url(${Pacifico}) format('woff2');
}

@font-face {
  font-family: 'Play Fair Italic';
  src: url(${PlayfairItalic}) format('woff2');
}

@font-face {
  font-family: 'Play Fair';
  src: url(${Playfair}) format('woff2');
}
`

export default FontStyles
