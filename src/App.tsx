import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { Container } from './App.styles'
import { useCartItemState } from './useCartItemState'
import ShoppingHome from './ShoppingHome/ShoppingHome'
import Header from './Header/Header'

const queryClient = new QueryClient()

const App: React.FC = () => {
  const { cartItems, handleAddToCart, handleRemoveFromCart } =
    useCartItemState()

  return (
    <Container>
      <QueryClientProvider client={queryClient}>
        <Header
          cartItems={cartItems}
          handleAddToCart={handleAddToCart}
          handleRemoveFromCart={handleRemoveFromCart}
        />
        <ShoppingHome handleAddToCart={handleAddToCart} />
      </QueryClientProvider>
    </Container>
  )
}

export default App
