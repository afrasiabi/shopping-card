import Header, { Props } from './Header'
import { render, screen } from '@testing-library/react'

const props: Props = {
  cartItems: [],
  handleAddToCart: jest.fn(),
  handleRemoveFromCart: jest.fn()
}

describe('Header', () => {
  it('should render header information correctly with no item in cart', () => {
    render(<Header {...props} />)
    expect(screen.getByTestId('appTitle')).toHaveTextContent('whee')
    expect(screen.getByTestId('appDesc')).toHaveTextContent(
      'The most definitive shape store in the world'
    )
    expect(screen.getByTestId('noItem')).toBeVisible()
  })

  it('should render correct header information with some items in cart', () => {
    render(
      <Header
        {...props}
        cartItems={[
          {
            id: 1,
            title: 'title1',
            description: 'abc',
            image: '',
            price: 10,
            amount: 2
          },
          {
            id: 2,
            title: 'title2',
            description: 'fhj',
            image: '',
            price: 10,
            amount: 3
          }
        ]}
      />
    )
    expect(screen.getByTestId('contentBadge')).toHaveTextContent('5')
  })
})
