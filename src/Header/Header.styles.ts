import styled from 'styled-components'
import IconButton from '@mui/material/IconButton'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'

export const HeaderContainer = styled.div`
  height: 100px;
  background-color: #cfd5dc;
  display: flex;
  position: relative;
  align-items: center;
  justify-content: space-between;
  &:after {
    background-image: linear-gradient(0, transparent 30px, #cfd5dc 31px),
      linear-gradient(135deg, #cfd5dc 16px, transparent 17px),
      linear-gradient(0, transparent 30px, #cfd5dc 31px),
      linear-gradient(225deg, #cfd5dc 16px, transparent 17px);
    background-size: 23px 40px;
    background-repeat: repeat-x;
    position: absolute;
    bottom: -20px;
    content: ' ';
    height: 30px;
    left: 0;
    right: 0;
  }
`

export const TextContainer = styled.div`
  display: flex;
  align-items: center;
`

export const BasketButton = styled(IconButton)`
  z-index: 1;
  margin-right: 1rem !important;
  background-color: black !important;
`

export const Title = styled.span`
  font-size: 2.5rem;
  margin-left: 1rem;
  font-family: 'Pacifico';
`

export const Description = styled.span`
  font-size: 1rem;
  margin-left: 1.4rem;
  color: #5a5a66;
  font-family: 'Play Fair Italic';
`

export const NoItem = styled.span`
  font-size: 1rem;
  color: #5a5a66;
  margin-right: 1.4rem;
  font-family: 'Play Fair Italic';
`

export const ShoppingCardStyle = styled(ShoppingCartIcon)`
  color: #cfd5dc;
`
