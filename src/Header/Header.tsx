import { useState } from 'react'
import ShoppingCart from '../ShoppingCart/ShoppingCart'
import Drawer from '@mui/material/Drawer'
import Badge from '@mui/material/Badge'
import {
  HeaderContainer,
  BasketButton,
  Title,
  Description,
  TextContainer,
  NoItem,
  ShoppingCardStyle
} from './Header.styles'
import { ItemType } from '../types'

export type Props = {
  cartItems: ItemType[]
  handleAddToCart: (item: ItemType) => void
  handleRemoveFromCart: (id: number) => void
}

const Header: React.FC<Props> = ({
  cartItems,
  handleAddToCart,
  handleRemoveFromCart
}) => {
  const [cartOpen, setCartOpen] = useState(false)

  const getTotalItems = (items: ItemType[]): number =>
    items.reduce((acc: number, item) => acc + item.amount, 0)

  const handleCloseDrawer = (): void => setCartOpen(false)
  const handleOpenDrawer = (): void => setCartOpen(true)

  return (
    <HeaderContainer>
      <TextContainer>
        <Title data-testid="appTitle">whee</Title>
        <Description data-testid="appDesc">
          The most definitive shape store in the world
        </Description>
      </TextContainer>
      <div>
        <Drawer anchor="right" open={cartOpen} onClose={handleCloseDrawer}>
          <ShoppingCart
            cartItems={cartItems}
            addToCart={handleAddToCart}
            removeFromCart={handleRemoveFromCart}
          />
        </Drawer>
        {getTotalItems(cartItems) === 0 && (
          <NoItem data-testid="noItem">No items in cart</NoItem>
        )}
        <BasketButton onClick={handleOpenDrawer}>
          <Badge
            badgeContent={getTotalItems(cartItems)}
            overlap="rectangular"
            color="error"
            data-testid="contentBadge"
          >
            <ShoppingCardStyle />
          </Badge>
        </BasketButton>
      </div>
    </HeaderContainer>
  )
}

export default Header
