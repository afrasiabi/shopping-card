import { FC, PropsWithChildren } from 'react'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { render } from '@testing-library/react'
import { server } from '../mocks/server'
import { rest } from 'msw'
import ShoppingHome from './ShoppingHome'
import '@testing-library/jest-dom'

const queryClient = new QueryClient()

const Wrapper: FC<PropsWithChildren> = ({ children }) => (
  <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
)

describe('Shopping home', () => {
  it('should call api and returns data correctly', () => {
    render(
      <Wrapper>
        <ShoppingHome handleAddToCart={() => {}} />
      </Wrapper>
    )
    // Todo: Expect if the api calls correctly and shows data

    expect(true).toBeTruthy()
  })

  test('should handle server error', async () => {
    server.use(
      rest.get('http://localhost:3001/api/items', (req, res, ctx) => {
        return res(ctx.status(500))
      })
    )

    render(
      <Wrapper>
        <ShoppingHome handleAddToCart={() => {}} />
      </Wrapper>
    )
    expect(true).toBeTruthy()

    // Todo: Expect if the api calls with error
  })
  // Todo: Test loading state
})
