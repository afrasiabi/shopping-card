import styled from 'styled-components'

export const Container = styled.div`
  margin-top: 2rem;
  display: flex;
  flex-direction: column;
  max-width: 742px;
  margin-left: auto;
  margin-right: auto;
`
