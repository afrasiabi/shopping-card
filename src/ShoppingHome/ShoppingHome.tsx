import { useQuery } from '@tanstack/react-query'
import StoreItem from '../StoreItem/StoreItem'
import LinearProgress from '@mui/material/LinearProgress'
import { Container } from './ShoppingHome.styles'
import { ItemType } from '../types'

type Props = {
  handleAddToCart: (item: ItemType) => void
}

const getItems = async (): Promise<ItemType[]> =>
  await (await fetch('http://localhost:3001/api/items')).json()

const ShoppingHome: React.FC<Props> = ({ handleAddToCart }) => {
  const { data, isLoading, error } = useQuery<ItemType[]>(['items'], getItems)

  if (isLoading) return <LinearProgress />
  if (error) return <div data-testid="errorMsg">Something went wrong ...</div>

  return (
    <Container>
      {data?.map((item) => (
        <StoreItem
          key={item.id}
          item={item}
          handleAddToCart={handleAddToCart}
        />
      ))}
    </Container>
  )
}

export default ShoppingHome
