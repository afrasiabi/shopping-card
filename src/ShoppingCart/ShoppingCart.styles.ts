import styled from 'styled-components'

export const Container = styled.div`
  font-family: 'Play Fair Italic';
  width: 500px;
  padding: 1rem;
`
