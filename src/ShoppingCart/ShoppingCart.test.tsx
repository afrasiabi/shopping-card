import ShoppingCart, { Props } from './ShoppingCart'
import { render, screen } from '@testing-library/react'

const props: Props = {
  cartItems: [],
  addToCart: jest.fn(),
  removeFromCart: jest.fn()
}

describe('Shopping cart', () => {
  it('should show correct total price without any item in cart', () => {
    render(<ShoppingCart {...props} />)
    expect(screen.getByTestId('totalPrice')).toHaveTextContent('0')
    expect(screen.getByTestId('noItem')).toHaveTextContent('No items in cart.')
  })

  it('should render correct total price with some items in cart', () => {
    render(
      <ShoppingCart
        {...props}
        cartItems={[
          {
            id: 1,
            title: 'title1',
            description: 'abc',
            image: '',
            price: 10,
            amount: 1
          },
          {
            id: 2,
            title: 'title2',
            description: 'fhj',
            image: '',
            price: 5,
            amount: 3
          }
        ]}
      />
    )
    expect(screen.getByTestId('totalPrice')).toHaveTextContent('25')
  })
})
