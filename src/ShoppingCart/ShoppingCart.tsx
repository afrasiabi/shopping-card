import CartItem from '../CartItem/CartItem'
import { Container } from './ShoppingCart.styles'
import { ItemType } from '../types'

export type Props = {
  cartItems: ItemType[]
  addToCart: (clickedItem: ItemType) => void
  removeFromCart: (id: number) => void
}

const ShoppingCart: React.FC<Props> = ({
  cartItems,
  addToCart,
  removeFromCart
}) => {
  const calculateTotal = (items: ItemType[]): number =>
    items.reduce((acc: number, item) => acc + item.amount * item.price, 0)

  return (
    <Container>
      <h2>Your Shopping Cart</h2>
      {cartItems.length === 0 ? (
        <p data-testid="noItem">No items in cart.</p>
      ) : null}
      {cartItems.map((item) => (
        <CartItem
          key={item.id}
          item={item}
          addToCart={addToCart}
          removeFromCart={removeFromCart}
        />
      ))}
      <h2 data-testid="totalPrice">
        Total: {calculateTotal(cartItems).toFixed(2)} €
      </h2>
    </Container>
  )
}

export default ShoppingCart
