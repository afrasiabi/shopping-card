import { ItemType } from '../types'
import {
  Container,
  AddButton,
  LeftContainer,
  RightContainer,
  TitleDesc,
  Price,
  Desc,
  ItemTitle
} from './StoreItem.styles'

export type Props = {
  item: ItemType
  handleAddToCart: (item: ItemType) => void
}

const Item: React.FC<Props> = ({ item, handleAddToCart }) => (
  <Container>
    <LeftContainer>
      <img src={item.image} alt={item.title} />
      <TitleDesc>
        <ItemTitle data-testid="itemTitle">{item.title}</ItemTitle>
        <Desc data-testid="itemDesc">{item.description}</Desc>
      </TitleDesc>
    </LeftContainer>
    <RightContainer>
      <Price data-testid="itemPrice">{item.price} €</Price>
      <AddButton data-testid="addToCart" onClick={() => handleAddToCart(item)}>
        ADD TO CART
      </AddButton>
    </RightContainer>
  </Container>
)

export default Item
