import StoreItem, { Props } from './StoreItem'
import { render, fireEvent, screen } from '@testing-library/react'

const props: Props = {
  item: {
    id: 1,
    title: 'title1',
    description: 'abc',
    image: '',
    price: 10,
    amount: 1
  },
  handleAddToCart: jest.fn()
}

describe('Store item', () => {
  const handleAddToCart = jest.fn()
  it('button should add to cart', () => {
    render(<StoreItem {...props} handleAddToCart={handleAddToCart} />)
    fireEvent.click(screen.getByTestId('addToCart'))
    expect(handleAddToCart).toBeCalledTimes(1)
  })

  it('should render item information correctly', () => {
    render(<StoreItem {...props} />)
    expect(screen.getByTestId('itemTitle')).toHaveTextContent('title1')
    expect(screen.getByTestId('itemDesc')).toHaveTextContent('abc')
    expect(screen.getByTestId('itemPrice')).toHaveTextContent('10')
  })
})
