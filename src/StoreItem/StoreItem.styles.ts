import styled from 'styled-components'
import Button from '@mui/material/Button'

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1rem;
  height: 100%;
  padding: 1rem;
  img {
    max-height: 200px;
    width: 70%;
    max-width: 200px;
    min-width: 100px;
    object-fit: cover;
  }
`

export const Price = styled.div`
  text-align: right;
  font-size: 2rem;
  font-family: 'Play Fair Italic';
  margin-bottom: 8px;
`

export const AddButton = styled(Button)`
  && {
    color: black;
    font-family: 'Play Fair';
    font-size: 1rem;
    font-weight: bold;
    border-color: blue;
    border: 1px solid black;
    border-radius: 0;
    width: 140px;
  }
`

export const LeftContainer = styled.div`
  display: flex;
  align-items: center;
`

export const RightContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 2rem;
  margin-left: 2rem;
`

export const ItemTitle = styled.h3`
  font-family: 'Play Fair Italic';
  font-style: italic;
  font-weight: bold;
  font-size: 2rem;
  margin-block-end: 0;
  margin-block-start: 0;
`

export const TitleDesc = styled.div`
  margin-left: 2rem;
`

export const Desc = styled.p`
  max-width: 150px;
  font-family: 'Play Fair Italic';
  font-size: 0.75rem;
`
