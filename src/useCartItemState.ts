import { useState } from 'react'
import { ItemType } from './types'

type CartItemState = {
  cartItems: ItemType[]
  handleAddToCart: (clickedItem: ItemType) => void
  handleRemoveFromCart: (id: number) => void
}

export const useCartItemState = (): CartItemState => {
  const [cartItems, setCartItems] = useState([] as ItemType[])

  const handleAddToCart = (clickedItem: ItemType): void => {
    setCartItems((prev) => {
      const isItemInCart = prev.find((item) => item.id === clickedItem.id)

      if (isItemInCart) {
        return prev.map((item) =>
          item.id === clickedItem.id
            ? { ...item, amount: item.amount + 1 }
            : item
        )
      }
      return [...prev, { ...clickedItem, amount: 1 }]
    })
  }

  const handleRemoveFromCart = (id: number): void => {
    setCartItems((prev) =>
      prev.reduce<ItemType[]>((acc, item) => {
        if (item.id === id) {
          if (item.amount === 1) return acc
          return [...acc, { ...item, amount: item.amount - 1 }]
        } else {
          return [...acc, item]
        }
      }, [])
    )
  }

  return {
    cartItems,
    handleAddToCart,
    handleRemoveFromCart
  }
}
