import styled from 'styled-components'
import Button from '@mui/material/Button'

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  font-family: 'Play Fair Italic';
  border-bottom: 1px solid lightblue;
  padding-bottom: 1rem;
  padding-top: 1rem;
  div {
    flex: 1;
  }
  h3 {
    margin-block-start: 0;
    margin-block-end: 0;
  }
  .information,
  .buttons {
    display: flex;
    justify-content: space-between;
  }
  img {
    max-width: 200px;
    object-fit: contain;
    margin-right: 2rem;
  }
`

export const AddRemoveBtn = styled(Button)`
  background-color: black !important;
  font-size: 1.5rem !important;
`
