import { ItemType } from '../types'
import { Container, AddRemoveBtn } from './CartItem.styles'

export type Props = {
  item: ItemType
  addToCart: (item: ItemType) => void
  removeFromCart: (id: number) => void
}

const CartItem: React.FC<Props> = ({ item, addToCart, removeFromCart }) => (
  <Container>
    <img src={item.image} alt={item.title} />
    <div>
      <h3 data-testid="itemTitle">{item.title}</h3>
      <div className="information">
        <p data-testid="itemPrice">Price: {item.price} €</p>
        <p data-testid="itemAmount">
          Total: {(item.amount * item.price).toFixed(2)} €
        </p>
      </div>
      <div className="buttons">
        <AddRemoveBtn
          size="small"
          disableElevation
          variant="contained"
          onClick={() => removeFromCart(item.id)}
          data-testid="removeBtn"
        >
          -
        </AddRemoveBtn>
        <p>{item.amount}</p>
        <AddRemoveBtn
          size="large"
          disableElevation
          variant="contained"
          onClick={() => addToCart(item)}
          data-testid="addBtn"
        >
          +
        </AddRemoveBtn>
      </div>
    </div>
  </Container>
)

export default CartItem
