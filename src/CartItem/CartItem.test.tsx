import CartItem, { Props } from './CartItem'
import { render, fireEvent, screen } from '@testing-library/react'

const props: Props = {
  item: {
    id: 1,
    title: 'title1',
    description: 'abc',
    image: '',
    price: 10,
    amount: 3
  },
  addToCart: jest.fn(),
  removeFromCart: jest.fn()
}

describe('Cart item', () => {
  const handleAddToCart = jest.fn()
  const handleRemoveFromCart = jest.fn()
  it('button should add to cart', () => {
    render(<CartItem {...props} addToCart={handleAddToCart} />)
    fireEvent.click(screen.getByTestId('addBtn'))
    expect(handleAddToCart).toBeCalledTimes(1)
  })

  it('button should remove from cart', () => {
    render(<CartItem {...props} removeFromCart={handleRemoveFromCart} />)
    fireEvent.click(screen.getByTestId('removeBtn'))
    expect(handleRemoveFromCart).toBeCalledTimes(1)
  })

  it('should render cart item information correctly', () => {
    render(<CartItem {...props} />)
    expect(screen.getByTestId('itemTitle')).toHaveTextContent('title1')
    expect(screen.getByTestId('itemAmount')).toHaveTextContent('3')
    expect(screen.getByTestId('itemPrice')).toHaveTextContent('10')
  })
})
