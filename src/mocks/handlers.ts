import { rest } from 'msw'
import dataJson from '../../data/items.json'

export const handlers = [
  rest.get('http://localhost:3001/api/items', (req, res, ctx) => {
    return res(ctx.json(dataJson))
  })
]
